function smooth = gauss_blur(img)
    x = linspace(-2, 2, 5);
    sigma = 1;
    gauss_filter = 1/(sqrt(2*pi)*sigma)*exp(-x.^2/(2*sigma^2));
    
    smooth_x = conv2(img, gauss_filter, 'same');
    smooth = conv2(smooth_x, gauss_filter.', 'same');
end

