function [I_x,I_y] = grad2d(img)
    dx_filter = linspace(0.5, -0.5, 3);
    I_x = conv2(img, dx_filter, 'same');
    
    dy_filter = dx_filter.';
    I_y = conv2(img, dy_filter, 'same');
end

