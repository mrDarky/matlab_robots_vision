function loc = nmsup(R, r, thresh)
    [sy,sx] = size(R);
    
    R_thresh = R(R > thresh);
    sorted_R_thresh = sort(R_thresh, 'descend');
    
    size_sorted_R = size(sorted_R_thresh);
    
    loc_arr = cell(size_sorted_R);
    for a = 1:size_sorted_R
        for x = 1:sx
            for y = 1:sy
                if R(y,x) == sorted_R_thresh(a)
                    loc_arr{a} = [x y];
                end
            end
        end
    end
    
    for i = 1:size_sorted_R
        for j = i+1:size_sorted_R
            if (loc_arr{i}(1) - loc_arr{j}(1))^2 + (loc_arr{i}(2) - loc_arr{j}(2))^2 <= r^2
                sorted_R_thresh(j) = 0;
            end
        end
    end
    
    final_R = sorted_R_thresh(sorted_R_thresh ~= 0);
    size_final_R = size(final_R);
    final_loc_arr = zeros(size_final_R(1), 2);
    
    counter = 1;
    for i = 1:size_sorted_R
        if sorted_R_thresh(i) ~= 0
            final_loc_arr(counter, 1) = loc_arr{i}(1);
            final_loc_arr(counter, 2) = loc_arr{i}(2);
            counter = counter + 1;
        end
    end

    
    loc = final_loc_arr;
end

