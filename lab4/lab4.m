img = imread('peppers.png');
img_gray = im2double(rgb2gray(img));

img_gray_smooth = gauss_blur(img_gray);
[I_x, I_y] = grad2d(img_gray_smooth);

I_xx = gauss_blur(I_x.^2);
I_yy = gauss_blur(I_y.^2);
I_xy = gauss_blur(I_x.*I_y);

k = 0.06;
R = (I_xx.*I_yy-I_xy.^2) - k*(I_xx+I_yy).^2;

r = 5;
thresh = 10000;

hc = nmsup(R,r,thresh);
 
figure()
imshow(img)
hold on;
plot(hc(:,1), hc(:,2), 'rx')
hold off;