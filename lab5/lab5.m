I1 = rgb2gray(imread('peppers.png'));

points1 = detectHarrisFeatures(I1);

[features1, points1] = extractFeatures(I1, points1);

loc1 = points1.Location;

[match, match_fwd, match_bkwd] = match_features(double(features1.Features), double(features1.Features));

H = ransac_homography(loc1(match(:,1),:), loc1(match(:,2),:));
