function [match, match_fwd, match_bkwd] = match_features(f1, f2)
    threshold = 0.7;

    [IDX, D] = knnsearch(f2, f1, 'k', 2);

    counter = 1;
    match_fwd = zeros();
    for i = 1:size(D,1)
        if D(i,1)/D(i,2) < threshold
            match_fwd(counter, 1) = i;
            match_fwd(counter, 2) = IDX(i, 1);
            counter = counter + 1;
        end
    end


    [IDX, D] = knnsearch(f1, f2, 'k', 2);

    counter = 1;
    match_bkwd = zeros();
    for i = 1:size(D,1)
        if D(i,1)/D(i,2) < threshold
            match_bkwd(counter, 1) = IDX(i, 1);
            match_bkwd(counter, 2) = i;
            counter = counter + 1;
        end
    end

    Lia = ismember(match_fwd, match_bkwd);
    
    match = zeros();
    counter = 1;
    for i = 1:size(Lia, 1)
        if Lia(i, 1) == 1
            match(counter, 1) = match_fwd(i, 1);
            match(counter, 2) = match_fwd(i, 2);
            counter = counter + 1;
        end
    end
   
end

