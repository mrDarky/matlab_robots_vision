function best_H = ransac_homography(p1, p2)
    thresh = sqrt(2);
    p = 1-1e-4;
    w = 0.5;
    n = 4;
    k = log(1-p)/log(1-w^n);
    num_pts = size(p1,1);
    ind = 1:num_pts;
    best_inliers = 4;
    best_H = eye(3);
    
    for iter = 1:k
        perm = randperm(num_pts);
        rand_index = perm(1:n);
        
        p1_sample = zeros(n, 2);
        p2_sample = zeros(n, 2);
        for x = 1:n
            p1_sample(x, 1) = p1(rand_index(x), 1);
            p1_sample(x, 2) = p1(rand_index(x), 2);
            
            p2_sample(x, 1) = p2(rand_index(x), 1);
            p2_sample(x, 2) = p2(rand_index(x), 2);
        end

        H = compute_homography(p1_sample, p2_sample);
	
        q2 = [p2(:, 1).'; p2(:, 2).'; ones(1, size(p2, 1))];
        q2_h = H*q2;
        q2_h = q2_h./[q2_h(3, :); q2_h(3,:); q2_h(3, :)];
        p2_h(:, 1) = (q2_h(1, :))';
        p2_h(:, 2) = (q2_h(2, :))';

        dist = sqrt((p2_h(:, 1) - p1(:, 1)).^2 + (p2_h(:, 2) - p1(:, 2)).^2);
        num_inliers = length(ind(dist < thresh));
		
        if num_inliers > best_inliers
            best_inliers = num_inliers;
            best_H = H;
        end
    end
    
end

