function H = compute_homography(p1, p2)

[points_count, dimention] = size(p1);
A = zeros(points_count*dimention, 9);

for i = 1:points_count
    row = dimention*i - 1;

    vx = p1(i, 1);
    vy = p1(i, 2);

    ux = p2(i, 1);
    uy = p2(i, 2);
    
    A(row, 1:9) = [ux, uy, 1, 0, 0, 0, -ux*vx, -uy*vx, -vx];
    A(row+1, 1:9) = [0, 0, 0, ux, uy, 1, -ux*vy, -uy*vy, -vy];
end

[U D V] = svd(A);
x = V(:, end) / V(end, end);
H = reshape(x, 3, 3)';

end

