function snr = compute_snr(I, Id)

    % Input:
    % I: the original image
    % Id: the approximated (noisy) image
    % Output:
    % snr: signal-to-noise ratio
    
    % Please follow the instructions in the comments to fill in the missing commands.    

    % 1) Compute the noise image (original image minus the approximation)
    Noise_I = I - Id;
    %log(Noise_I)
    % 2) Compute the Frobenius norm of the noise image
    Frobenius_norm_Id = norm(Noise_I, 'fro');
    %log(Frobenius_norm_Id)
    % 3) Compute the Frobenius norm of the original image
    Frobenius_norm_I = norm(I, 'fro');
    %log(Frobenius_norm_I);
    % 4) Compute SNR
    snr = -20*log10(Frobenius_norm_Id/Frobenius_norm_I);
end